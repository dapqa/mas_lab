package mas.gui.map;

/**
 * @author Nechaev Alexander
 */
public enum ClickMode {

    ADD_WALL, ADD_AGENT, SET_GOAL

}
