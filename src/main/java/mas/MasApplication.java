package mas;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mas.impl.Facilitator;

/**
 * @author Nechaev Alexander
 */
public class MasApplication extends Application {

    private static final String layout = "MainPane.fxml";

    public static final String TITLE = "Мультиагентная система";

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader mainPaneLoader = new FXMLLoader(getClass().getResource(layout));
        Scene scene = new Scene(mainPaneLoader.load());

        primaryStage.setScene(scene);
        primaryStage.setTitle(TITLE);
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        // При закрытии приложения все потоки агентов нужно завершить.
        Facilitator.getInstance().killAgentTasks();
    }
}
