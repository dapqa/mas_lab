package mas.impl.environment;

import mas.core.agent.AgentCommunication;
import mas.core.environment.Environment;
import mas.core.item.Goal;
import mas.core.item.Wall;
import mas.core.util.Coordinates;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nechaev Alexander
 */
public class EnvironmentImpl implements Environment {

    private List<AgentCommunication> agents = new ArrayList<>();
    private List<Wall> walls = new ArrayList<>();
    private Goal goal = null;

    @Override
    public List<AgentCommunication> getAgents() {
        return Collections.unmodifiableList(agents);
    }

    @Override
    public List<Wall> getWalls() {
        return Collections.unmodifiableList(walls);
    }

    @Override
    public Goal getGoal() {
        return goal;
    }

    @Override
    public void addAgent(AgentCommunication agent) {
        agents.add(agent);
    }

    @Override
    public boolean removeAgent(Coordinates position) {
        return agents.removeIf(agent -> agent.getPosition().equals(position));
    }

    @Override
    public void addWall(Wall wall) {
        walls.add(wall);
    }

    @Override
    public boolean removeWall(Coordinates position) {
       return walls.removeIf(wall -> wall.getPosition().equals(position));
    }

    @Override
    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    @Override
    public boolean removeGoal(Coordinates position) {
        if (goal == null) {
            return false;
        }

        if (position == null || goal.getPosition().equals(position)) {
            goal = null;
            return true;
        }

        return false;
    }
}
