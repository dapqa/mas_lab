package mas.core.util.exception;

/**
 * Исключение, которое может сгенерировать агент, если до цели не
 * существует пути.
 *
 * @author Nechaev Alexander
 */
public class PanicException extends Exception {
}
