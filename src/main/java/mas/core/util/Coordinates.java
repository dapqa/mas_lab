package mas.core.util;

/**
 * Класс для описания позиции объекта на поле (в двумерной среде)
 *
 * @author Nechaev Alexander
 */
public class Coordinates {

    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    /**
     * Поскольку поле имеет размер 20х20, такой хэш-код будет уникален
     * @return Хэш-код
     */
    @Override
    public int hashCode() {
        return 100 * x + y;
    }
}
