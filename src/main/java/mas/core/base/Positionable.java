package mas.core.base;

import mas.core.util.Coordinates;

/**
 * Базовый интерфейс для объекта на поле (в двумерной среде)
 *
 * @author Nechaev Alexander
 */
public interface Positionable {

    /**
     * Возвращает координаты объекта
     * @return Координаты объекта
     */
    Coordinates getPosition();

}
