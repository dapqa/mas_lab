package mas.impl;

import mas.core.agent.Agent;
import mas.core.base.Positionable;
import mas.core.environment.Environment;
import mas.core.environment.EnvironmentSensors;
import mas.core.util.Coordinates;
import mas.core.util.exception.PanicException;
import mas.gui.map.MoveListener;
import mas.impl.agent.AgentImpl;
import mas.impl.environment.EnvironmentImpl;
import mas.impl.item.GoalImpl;
import mas.impl.item.WallImpl;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Класс-посредник между средой и агентами. Синглтон. Организует редактирование среды;
 * настраивает, запускает и останавливает работу агентов
 *
 * @author Nechaev Alexander
 */
public class Facilitator {

    private Environment environment;

    // Поскольку среда предоставляет списки агентов и цель только для чтения информации,
    // в посреднике хранятся копии ссылок на агентов и цель, созданные эти посредником.
    // Через них можно вызывать методы для модификации.
    private List<Agent> agents = new ArrayList<>();
    private GoalImpl goal;

    // Каждый агент работает в своем потоке. Поля ниже - для управления этими потоками.
    private ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
    private Map<Integer, ScheduledFuture<?>> agentTasks = new HashMap<>();
    private int delay = 250;

    private final Object lock = new Object();

    // Слушатель из GUI для перерисовки поля при передвижении агентов
    private MoveListener moveListener;

    private Facilitator() {
    }

    private static Facilitator INSTANCE = new Facilitator();
    public static Facilitator getInstance() {
        return INSTANCE;
    }

    /**
     * Очищает среду и всю информацию о ней в посреднике
     */
    public void reset() {
        environment = new EnvironmentImpl();
        agents.clear();
        goal = null;

        agentTasks.forEach((id, task) -> task.cancel(true));
        agentTasks.clear();
    }

    public void setMoveListener(MoveListener moveListener) {
        this.moveListener = moveListener;
    }

    private void checkEnvironment() {
        if (environment == null) {
            reset();
        }
    }

    /**
     * Создает или удаляет агента на указанной позиции. При создании агента создается поток для его работы.
     * Если на этой позиции есть объект другого типа, он будет удалён.
     * У всех агентов (кроме созданного или удалённого) вызывается onEnvironmentChange.
     * @param position Позиция агента
     */
    public void toggleAgent(Coordinates position) {
        checkEnvironment();

        if (!environment.removeAgent(position)) {
            environment.removeWall(position);
            environment.removeGoal(position);

            Agent agent = new AgentImpl(environment, position);
            environment.addAgent(agent);
            agents.forEach(Agent::onEnvironmentChange);

            agents.add(agent);
            scheduleAgentWork(agent);
        } else {
            removeAgentInfo(position);
        }
    }

    private void removeAgentInfo(Coordinates position) {
        for (Agent agent : agents) {
            if (agent.getPosition().equals(position)) {
                agentTasks.get(agent.getId()).cancel(true);
                agentTasks.remove(agent.getId());
            }
        }
        agents.removeIf(agent -> agent.getPosition().equals(position));
        agents.forEach(Agent::onEnvironmentChange);
    }

    /**
     * Создает или удаляет стену на указанной позиции.
     * Если на этой позиции есть объект другого типа, он будет удалён.
     * У всех агентов вызывается onEnvironmentChange.
     * @param position Позиция стены
     */
    public void toggleWall(Coordinates position) {
        checkEnvironment();

        if (!environment.removeWall(position)) {
            environment.removeAgent(position);
            environment.removeGoal(position);
            environment.addWall(new WallImpl(position));
        }

        agents.forEach(Agent::onEnvironmentChange);
    }

    /**
     * Создает или удаляет цель на указанной позиции.
     * Если на этой позиции есть объект другого типа, он будет удалён.
     * У всех агентов вызывается onEnvironmentChange.
     * @param position Позиция цели
     */
    public void toggleGoal(Coordinates position) {
        checkEnvironment();

        if (!environment.removeGoal(position)) {
            environment.removeAgent(position);
            environment.removeWall(position);
            environment.setGoal(goal = new GoalImpl(position));
        } else {
            goal = null;
        }

        agents.forEach(Agent::onEnvironmentChange);
    }

    public EnvironmentSensors getEnvironment() {
        checkEnvironment();
        return environment;
    }

    /**
     * Завершает работу всех потоков агентов
     */
    public void killAgentTasks() {
        agentTasks.values().forEach(t -> t.cancel(true));
        executor.shutdown();
    }

    /**
     * Пересоздает потоки агентов, чтобы они работали по указанному расписанию (задаваемому полем delay)
     */
    public void rescheduleAgentsWork() {
        agentTasks.values().forEach(t -> t.cancel(false));
        agentTasks.clear();
        agents.forEach(this::scheduleAgentWork);
    }

    /**
     * Создает поток для работы агента. Он будет работать по указанному расписанию (задаваемому полем delay).
     * @param agent Агент
     */
    private void scheduleAgentWork(Agent agent) {
        ScheduledFuture<?> task = executor.scheduleAtFixedRate(() -> {
            synchronized (lock) {
                try {
                    if (environment.getGoal() == null) {
                        return;
                    }

                    Coordinates from = agent.getPosition();
                    agent.move();
                    Coordinates to = agent.getPosition();

                    Coordinates goalPosition = Optional.ofNullable(environment.getGoal())
                            .map(Positionable::getPosition)
                            .orElse(null);

                    // Если агент достиг цели, он удаляется
                    if (goalPosition != null && goalPosition.equals(to)) {
                        environment.removeAgent(to);
                        removeAgentInfo(to);
                        goal.incReachedCount();
                    }

                    if (moveListener != null) {
                        moveListener.onMove(from, to);
                    }
                } catch (PanicException e) {
                    // Тут можно что-то сделать, если нужно
                }
            }
        }, 0, delay, TimeUnit.MILLISECONDS);

        agentTasks.put(agent.getId(), task);
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}