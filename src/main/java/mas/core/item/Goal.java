package mas.core.item;

import mas.core.base.Positionable;

/**
 * Интерфейс для цели агентов, к которой они должны добраться
 *
 * @author Nechaev Alexander
 */
public interface Goal extends Positionable {

    /**
     * Возвращает количество агентов, достигших этой цели
     * @return Количество агентов, достигших этой цели
     */
    int getReachedCount();

}
