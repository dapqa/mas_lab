package mas.core.environment;

/**
 * @author Nechaev Alexander
 */
public interface Environment extends EnvironmentSensors, EnvironmentInitializable {
}
