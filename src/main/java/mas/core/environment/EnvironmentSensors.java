package mas.core.environment;

import mas.core.agent.AgentCommunication;
import mas.core.item.Goal;
import mas.core.item.Wall;

import java.util.List;

/**
 * Интерфейс среды, предоставляющей агентам информацию
 * о состоянии среды
 *
 * @author Nechaev Alexander
 */
public interface EnvironmentSensors {

    int WIDTH = 20;
    int HEIGHT = 20;

    List<AgentCommunication> getAgents();

    List<Wall> getWalls();

    Goal getGoal();

}
