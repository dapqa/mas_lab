package mas.core.agent;

import mas.core.base.Positionable;

/**
 * Интерфейс для общения агентов между собой
 *
 * @author Nechaev Alexander
 */
public interface AgentCommunication extends Positionable {

    /**
     * Возвращает уникальный номер агента
     * @return Уникальный номер агента
     */
    int getId();

}
