package mas.impl.item;

import mas.core.item.Wall;
import mas.core.util.Coordinates;
import mas.impl.base.PositionableImpl;

/**
 * @author Nechaev Alexander
 */
public class WallImpl extends PositionableImpl implements Wall {

    public WallImpl(Coordinates coordinates) {
        super(coordinates);
    }

}
