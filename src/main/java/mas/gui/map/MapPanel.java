package mas.gui.map;

import mas.core.agent.AgentCommunication;
import mas.core.environment.Environment;
import mas.core.environment.EnvironmentSensors;
import mas.core.item.Goal;
import mas.core.item.Wall;
import mas.core.util.Coordinates;
import mas.impl.Facilitator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Nechaev Alexander
 */
public class MapPanel extends JPanel {

    private static final int CELL_SIZE = 30;
    private static final int CELL_BORDER = 2;
    private static final int TEXT_PADDING = 5;
    private static final int TEXT_SIZE = 12;

    private static final Color CELL_COLOR = new Color(0xDD, 0xDD, 0xDD);
    private static final Color BORDER_COLOR = Color.WHITE;

    private static final Color WALL_COLOR = Color.BLACK;
    private static final Color AGENT_COLOR = new Color(0x0, 0xAA, 0x0);
    private static final Color GOAL_COLOR = new Color(0x0, 0x0, 0xDD);

    private static final Color TEXT_COLOR = Color.WHITE;

    private ClickMode clickMode = ClickMode.ADD_WALL;

    public MapPanel() {
        setMinimumSize(new Dimension(CELL_SIZE * Environment.WIDTH, CELL_SIZE * Environment.HEIGHT));
        addListeners();
        makeMoveListener();
    }

    private void addListeners() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int cellX = e.getX() / CELL_SIZE;
                int cellY = e.getY() / CELL_SIZE;

                Facilitator facilitator = Facilitator.getInstance();
                switch (clickMode) {
                    case ADD_WALL:
                        facilitator.toggleWall(new Coordinates(cellX, cellY));
                        break;
                    case ADD_AGENT:
                        facilitator.toggleAgent(new Coordinates(cellX, cellY));
                        break;
                    case SET_GOAL:
                        facilitator.toggleGoal(new Coordinates(cellX, cellY));
                        break;
                }

                MapPanel.this.repaint();
            }

        });
    }

    private void makeMoveListener() {
        Facilitator.getInstance().setMoveListener((from, to) -> {
            if (!from.equals(to)) {
                SwingUtilities.invokeLater(this::repaint);
            }
        });
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        // Сначала рисуется поле
        drawMesh(g);

        EnvironmentSensors environment = Facilitator.getInstance().getEnvironment();

        // На поле рисуются агенты
        for (AgentCommunication agent : environment.getAgents()) {
            drawObject(agent.getPosition(), AGENT_COLOR, g);
            drawText(agent.getPosition(), Integer.toString(agent.getId()), g);
        }

        // Потом рисуются стены
        for (Wall wall : environment.getWalls()) {
            drawObject(wall.getPosition(), WALL_COLOR, g);
        }

        // И при наличии цели рисуется цель
        Goal goal = environment.getGoal();
        if (goal != null) {
            drawObject(goal.getPosition(), GOAL_COLOR, g);
            drawText(goal.getPosition(), Integer.toString(goal.getReachedCount()), g);
        }
    }

    private void drawMesh(Graphics g) {
        g.setColor(BORDER_COLOR);
        g.fillRect(
                0, 0,
                Environment.WIDTH * CELL_SIZE, Environment.HEIGHT * CELL_SIZE
        );

        g.setColor(CELL_COLOR);
        for (int i = 0; i < Environment.HEIGHT; i++) {
            for (int j = 0; j < Environment.WIDTH; j++) {
                g.fillRect(
                        j * CELL_SIZE + CELL_BORDER, i * CELL_SIZE + CELL_BORDER,
                        CELL_SIZE - CELL_BORDER * 2, CELL_SIZE - CELL_BORDER * 2
                );
            }
        }
    }

    private void drawObject(Coordinates position, Color color, Graphics g) {
        g.setColor(color);
        g.fillRect(
                position.getX() * CELL_SIZE + CELL_BORDER, position.getY() * CELL_SIZE + CELL_BORDER,
                CELL_SIZE - CELL_BORDER * 2, CELL_SIZE - CELL_BORDER * 2
        );
    }

    private void drawText(Coordinates position, String text, Graphics g) {
        g.setColor(TEXT_COLOR);
        g.setFont(new Font("Arial", Font.BOLD, TEXT_SIZE));
        g.drawString(text, position.getX() * CELL_SIZE + TEXT_PADDING, position.getY() * CELL_SIZE + TEXT_PADDING + TEXT_SIZE);
    }

    public void setClickMode(ClickMode clickMode) {
        this.clickMode = clickMode;
    }
}