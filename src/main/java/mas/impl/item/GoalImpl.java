package mas.impl.item;

import mas.core.item.Goal;
import mas.core.util.Coordinates;
import mas.impl.base.PositionableImpl;

/**
 * @author Nechaev Alexander
 */
public class GoalImpl extends PositionableImpl implements Goal {

    private int reachedCount = 0;

    public GoalImpl(Coordinates coordinates) {
        super(coordinates);
    }

    public void incReachedCount() {
        reachedCount++;
    }

    @Override
    public int getReachedCount() {
        return reachedCount;
    }

}
