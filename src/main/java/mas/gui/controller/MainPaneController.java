package mas.gui.controller;

import javafx.embed.swing.SwingNode;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.layout.AnchorPane;
import mas.gui.map.ClickMode;
import mas.gui.map.MapPanel;
import mas.impl.Facilitator;

/**
 * @author Nechaev Alexander
 */
public class MainPaneController {

    @FXML
    public AnchorPane mapPane;
    @FXML
    public RadioButton addWallRadioButton;
    @FXML
    public RadioButton addAgentRadioButton;
    @FXML
    public RadioButton setGoalRadioButton;
    @FXML
    public Slider speedSlider;
    @FXML
    public Button changeSpeedButton;
    @FXML
    public Button resetButton;

    private MapPanel mapPanel;

    @FXML
    public void initialize() {
        initMapPanel();
        initMapControl();
        initSpeedControl();
    }

    private void initMapPanel() {
        SwingNode swingNode = new SwingNode() {
            @Override
            public boolean isResizable() {
                return false;
            }
        };

        mapPanel = new MapPanel();
        swingNode.setContent(mapPanel);
        mapPane.getChildren().add(swingNode);
        swingNode.resize(mapPanel.getWidth(), mapPanel.getHeight());
    }

    private void initMapControl() {
        addWallRadioButton.setOnAction(event -> mapPanel.setClickMode(ClickMode.ADD_WALL));
        addAgentRadioButton.setOnAction(event -> mapPanel.setClickMode(ClickMode.ADD_AGENT));
        setGoalRadioButton.setOnAction(event -> mapPanel.setClickMode(ClickMode.SET_GOAL));

        mapPanel.setClickMode(ClickMode.ADD_WALL);

        resetButton.setOnAction(event -> {
            Facilitator.getInstance().reset();
            mapPanel.repaint();
        });
    }

    private void initSpeedControl() {
        Facilitator facilitator = Facilitator.getInstance();

        speedSlider.setValue(facilitator.getDelay());
        changeSpeedButton.setOnAction(event -> {
            facilitator.setDelay((int) speedSlider.getValue());
            facilitator.rescheduleAgentsWork();
        });
    }

}
