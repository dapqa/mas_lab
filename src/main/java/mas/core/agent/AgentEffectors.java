package mas.core.agent;

import mas.core.util.exception.PanicException;

/**
 * Интерфейс для действий агента, меняющих состояние системы в целом
 *
 * @author Nechaev Alexander
 */
public interface AgentEffectors {

    /**
     * Если есть цель, то агент продвигается к цели
     * @throws PanicException Если есть цель, но до неё невозможно добраться
     */
    void move() throws PanicException;

    /**
     * Вызывается, когда в среде происходит изменение (добавление/удаление цели, стены, другого агента)
     */
    void onEnvironmentChange();

}
