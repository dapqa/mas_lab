package mas.core.item;

import mas.core.base.Positionable;

/**
 * @author Nechaev Alexander
 */
public interface Wall extends Positionable {
}
