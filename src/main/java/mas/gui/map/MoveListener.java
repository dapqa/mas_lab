package mas.gui.map;

import mas.core.util.Coordinates;

/**
 * Интерфейс для описания слушателя перемещения агента.
 * Используется для перерисовки поля при перемещениях агентов.
 *
 * @author Nechaev Alexander
 */
@FunctionalInterface
public interface MoveListener {

    void onMove(Coordinates from, Coordinates to);

}
