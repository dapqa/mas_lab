package mas.core.environment;

import mas.core.agent.AgentCommunication;
import mas.core.item.Goal;
import mas.core.item.Wall;
import mas.core.util.Coordinates;

/**
 * Интерфейс для редактирования среды
 *
 * @author Nechaev Alexander
 */
public interface EnvironmentInitializable {

    void addAgent(AgentCommunication agent);

    boolean removeAgent(Coordinates position);

    void addWall(Wall wall);

    boolean removeWall(Coordinates position);

    void setGoal(Goal goal);

    boolean removeGoal(Coordinates coordinates);

}