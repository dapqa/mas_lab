package mas.impl.agent;

import mas.core.agent.Agent;
import mas.core.base.Positionable;
import mas.core.environment.EnvironmentSensors;
import mas.core.util.Coordinates;
import mas.core.util.exception.PanicException;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Nechaev Alexander
 */
public class AgentImpl implements Agent {

    private static int nextId = 1;

    private EnvironmentSensors environment;

    private int id;
    private Coordinates position;

    // Путь пересчитывается только по необходимости
    private boolean madeFirstCalculation = false;
    private final LinkedList<Coordinates> route = new LinkedList<>();

    private static final int INFINITY = 10000;
    private static final int[][] REACHABLE = new int[][] {
            new int[] { -1, 0 },
            new int[] { 0, -1 },
            new int[] { 1, 0 },
            new int[] { 0, 1 }
    };

    public AgentImpl(EnvironmentSensors environment, Coordinates position) {
        this.environment = environment;
        this.position = position;

        this.id = nextId++;
    }

    @Override
    public void move() throws PanicException {
        if (!madeFirstCalculation) {
            calcRoute();
            madeFirstCalculation = true;
        }

        if (route.isEmpty()) {
            Coordinates goalPosition = Optional.ofNullable(environment.getGoal())
                    .map(Positionable::getPosition)
                    .orElse(null);

            // Если путь пустой, но есть цель и агент не достиг её - значит, пути не существует
            if (goalPosition != null && !position.equals(goalPosition)) {
                throw new PanicException();
            }

            return;
        }

        // Перемещение происходит, если в целевой ячейке нет другого агента
        if (nextCellIsFree()) {
            position = route.getFirst();
            route.removeFirst();
        }
    }

    @Override
    public void onEnvironmentChange() {
        // При добавлении/удалении объектов на поле путь нужно пересчитать
        calcRoute();
        madeFirstCalculation = true;
    }

    /**
     * Очищает текущий путь и, если есть цель, создает путь к ней.
     * Если есть цель, но пути нет, то путь остается пустым.
     */
    private void calcRoute() {
        route.clear();

        Coordinates goalPosition = Optional.ofNullable(environment.getGoal())
                .map(Positionable::getPosition)
                .orElse(null);

        if (goalPosition == null) {
            return;
        }

        // Для поиска пути используется волновой алгоритм
        int[][] wavedMap = new int[EnvironmentSensors.HEIGHT][EnvironmentSensors.WIDTH];
        makeWave(wavedMap, goalPosition);
        fillRoute(wavedMap, goalPosition);
    }

    /**
     * Заполняет массив волной из указанной точки. Волна не может проходить сквозь стены.
     * В недостижимых клетках будет стоять значение, задаваемое константой INFINITY.
     * @param mapToWave Массив для заполненения волной
     * @param goalPosition Центр волны
     */
    private void makeWave(int[][] mapToWave, Coordinates goalPosition) {
        for (int i = 0; i < mapToWave.length; i++) {
            for (int j = 0; j < mapToWave[i].length; j++) {
                mapToWave[i][j] = INFINITY;
            }
        }
        mapToWave[goalPosition.getY()][goalPosition.getX()] = 0;

        Set<Coordinates> wallPositions = environment.getWalls().stream()
                .map(Positionable::getPosition)
                .collect(Collectors.toSet());

        Set<Coordinates> cellsToVisit = new HashSet<>();
        Set<Coordinates> visitedCells = new HashSet<>();
        cellsToVisit.add(goalPosition);
        int distance = 1;
        while (!cellsToVisit.isEmpty()) {
            Set<Coordinates> newCellsToVisit = new HashSet<>();
            for (Coordinates cell : cellsToVisit) {
                for (int[] offset : REACHABLE) {
                    Coordinates cellWithOffset = new Coordinates(cell.getX() + offset[0], cell.getY() + offset[1]);
                    if (isOutOfMap(cellWithOffset) || visitedCells.contains(cellWithOffset) || wallPositions.contains(cellWithOffset)) {
                        continue;
                    }

                    mapToWave[cellWithOffset.getY()][cellWithOffset.getX()] = distance;

                    if (!wallPositions.contains(cellWithOffset) && !visitedCells.contains(cellWithOffset)) {
                        newCellsToVisit.add(cellWithOffset);
                    }
                }

                visitedCells.add(cell);
            }

            cellsToVisit = newCellsToVisit;
            distance++;
        }
    }

    /**
     * Восстанавливает путь от текущего положения до указанной клетки по волновому алгоритму.
     * Если пути не существует, путь будет пустым.
     * @param wavedMap Массив, заполненный волной из указанной целевой клетки
     * @param goalPosition Целевая клетка
     */
    private void fillRoute(int[][] wavedMap, Coordinates goalPosition) {
        Coordinates curPosition = new Coordinates(position.getX(), position.getY());

        for(;;) {
            int curValue = wavedMap[curPosition.getY()][curPosition.getX()];
            int minValue = curValue;
            Coordinates nextPosition = null;

            for (int[] offset : REACHABLE) {
                Coordinates position = new Coordinates(curPosition.getX() + offset[0], curPosition.getY() + offset[1]);;
                if (isOutOfMap(position)) {
                    continue;
                }

                int value = wavedMap[position.getY()][position.getX()];
                if (value < minValue) {
                    minValue = value;
                    nextPosition = position;
                }
            }

            if (curValue <= minValue) {
                break;
            }

            route.add(nextPosition);
            curPosition = nextPosition;
        }

        if (!goalPosition.equals(curPosition)) {
            route.clear();
        }
    }

    private boolean isOutOfMap(Coordinates cell) {
        return cell.getX() < 0 || cell.getX() >= EnvironmentSensors.WIDTH || cell.getY() < 0 || cell.getY() >= EnvironmentSensors.HEIGHT;
    }

    /**
     * Проверяет, нет ли агента в следующей по расчитаному пути клетке.
     * Для проверки спрашивается позиция у всех агентов из среды.
     * @return true, если нет агента в следующей по расчитаному пути клетке
     */
    private boolean nextCellIsFree() {
        if (route.isEmpty()) {
            return false;
        }

        Coordinates next = route.get(0);
        return environment.getAgents().stream().map(Positionable::getPosition).noneMatch(pos -> pos.equals(next));
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Coordinates getPosition() {
        return position;
    }

}
