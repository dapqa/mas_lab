package mas.core.agent;

/**
 * @author Nechaev Alexander
 */
public interface Agent extends AgentCommunication, AgentEffectors {
}
