package mas.impl.base;

import mas.core.base.Positionable;
import mas.core.util.Coordinates;

/**
 * @author Nechaev Alexander
 */
public abstract class PositionableImpl implements Positionable {

    private Coordinates coordinates;

    public PositionableImpl(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public Coordinates getPosition() {
        return coordinates;
    }

}
